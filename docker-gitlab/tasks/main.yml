- name:               "Create gitlab directory {{ item }}"
  file:
    path:             "{{ item }}"
    state:            "directory"
    mode:             "0755"
  with_items:
    - "/srv/gitlab"
    - "/srv/gitlab/config"
    - "/srv/gitlab/logs"
    - "/srv/gitlab/data"

- name:               "Create network for gitlab containers and add the web proxy"
  docker_network:
    name:             "gitlab-net"
    connected:
      - "{{ reverse_proxy_name }}"
    appends:          "yes"

- name:               "Run gitlab-ce container"
  docker_container:
    name:             "gitlab"
    image:            "gitlab/gitlab-ce:latest"
    hostname:         "{{ gitlab.domain }}"
    networks:
      - name:         "gitlab-net"
      - name:         "bridge"
    env:
      # Configure Gitlab via env var - https://docs.gitlab.com/ee/install/docker.html#pre-configure-docker-container
      GITLAB_OMNIBUS_CONFIG: "{{ lookup('template', 'gitlab_omnibus_config.j2') | trim() }}"
    ports:
      - "22:22"
    volumes:
      - "/srv/gitlab/config:/etc/gitlab"
      - "/srv/gitlab/logs:/var/log/gitlab"
      - "/srv/gitlab/data:/var/opt/gitlab"
    state:            "started"
    restart_policy:   "unless-stopped"
    labels:
      traefik.enable:                                         "true"

      traefik.http.routers.gitlab.rule:                    "Host(`{{ gitlab.domain }}`, `{{ gitlab.registry }}`)"
      traefik.http.routers.gitlab.entrypoints:             "web,websecure"
      traefik.http.routers.gitlab.tls.certresolver:        "mythicdns"
      traefik.http.services.gitlab.loadbalancer.server.port:  "80"

- name:               "Create backups into /var/opt/gitlab/backups"
  cron:
    name:             "Create daily dump"
    user:             "root"
    state:            "present"
    hour:             "23"
    minute:           "00"
    job:              "docker exec -t gitlab gitlab-rake gitlab:backup:create CRON=1 SKIP=registry,artifacts"

- name:               "Don't back up most of Gitlab's raw data"
  copy:
    content:          ""
    dest:             "/srv/gitlab/data/DO_NOT_BACKUP"
