This role sets up a host to generate and renew LetsEncrypt SSL certificates. It
uses [lego](https://github.com/go-acme/lego) via docker.

It expects to be using LetsEncrypt's DNS authentication with Mythic Beasts as a
provider.

It expects the following hostvars setup:

```
lego:
  mythicbeasts:
    api_user: apiid
    api_pass: apikey
  email: you@example.com
  domains:
    - "{{ ansible_fqdn }}"
  live: true
```

If set, `live: true` will use LetsEncrypt's live servers, otherwise it will use
the test server.
