fail2ban
========

This role installs and configures fail2ban on target systems.

It requires `host_vars` to be defined to tell it what fail2ban service jails to
activate, which ports to block on them, and what hosts to ignore so you
don't lock yourself out accidentally.

If port: is not specified for a service it will use the fail2ban defaults which
are sensible.

```
fail2ban:
  services:
    service:
    otherservice:
      port: [ port1, port2 ]
  ignoreip:
    - IPv4 addresses
    - IPv6 addresses
    - CIDR networks
```
