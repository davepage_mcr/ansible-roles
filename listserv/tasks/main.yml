# Role for a server with Sympa installed and customised

- name: Install swaks
  apt:
    name:     swaks
    state:    present
    cache_valid_time: 600

#################################################
# Set up exim MTA for incoming and outgoing mail

- name:     "Install exim4"
  apt:
    name:   exim4-daemon-heavy
    state:  present
    cache_valid_time: 600

- name:     "Install Exim template config"
  template:
    src:    "update-exim4.conf.conf"
    dest:   "/etc/exim4/update-exim4.conf.conf"
  notify:
    - update-exim4.conf
    - restart-exim

- name:     "Authentication for exim"
  lineinfile:
    path:   "/etc/exim4/passwd.client"
    regexp: '^{{ mail_proxy.host }}:'
    line:   '{{ mail_proxy.host }}:{{ mail_proxy.user }}:{{ mail_proxy.password }}'
  when:     mail_proxy is defined
  notify:
    - restart-exim

- name:     "Create key for DKIM"
  command:  "openssl genrsa -out /etc/exim4/{{ ansible_fqdn }}-private.pem 1024"
  args:
    creates:  "/etc/exim4/{{ ansible_fqdn }}-private.pem"

- name:     "Create cert for DKIM"
  command:  "openssl rsa -in /etc/exim4/{{ ansible_fqdn }}-private.pem -out /etc/exim4/{{ ansible_fqdn }}.pem -pubout"
  args:
    creates:  "/etc/exim4/{{ ansible_fqdn }}.pem"

- name:     "Register the pubkey for DKIM"
  command:  "awk '! /^---/ { t = t $0 } END { print t }' /etc/exim4/{{ ansible_fqdn }}.pem"
  register: pubkey_string
  changed_when: false

## TODO - use Mythic Beasts API to set the DKIM DNS record?

- name:     "Set permissions on dkim files"
  file:
    path:   "{{ item }}"
    group:  Debian-exim
    mode:   "g+r"
  with_items:
    - "/etc/exim4/{{ ansible_fqdn }}-private.pem"
    - "/etc/exim4/{{ ansible_fqdn }}.pem"

- name:     "Tell Exim to use DKIM"
  template:
    src:    "00_dkim_macros"
    dest:   "/etc/exim4/conf.d/main"
    owner:  root
    group:  root
    mode:   0644
  notify:
    - update-exim4.conf
    - restart-exim

# Sympa routers for main and vhost aliases

- name: "Install Sympa routers for exim"
  copy:
    dest: '/etc/exim4/conf.d/router/'
    mode: 0644
    owner: root
    group: root
    src: '{{ item }}'
  with_items:
    - 340_exim4-config_sympa_vhost_aliases
    - 350_exim4-config_sympa_aliases
  notify:
    - update-exim4.conf
    - restart-exim

#################################################
# Set up sympa - base install is not very exciting

- name:       "Configure debconf for sympa install"
  debconf:
    name:     sympa
    question: "{{ item.question }}"
    value:    "{{ item.value }}"
    vtype:    "{{ item.vtype }}"
  with_items:
    - { question: 'sympa/language', value: 'en', vtype: 'select' }
    - { question: 'sympa/database-type', value: 'mysql', vtype: 'select' }
    - { question: 'wwsympa/fastcgi', value: 'true', vtype: 'boolean' }
    - { question: 'wwsympa/webserver_type', value: 'Apache 2', vtype: 'select' }
    - { question: 'sympa/listmaster', value: 'dave@davepage.me.uk', vtype: 'string' }
    - { question: 'wwsympa/remove_spool', value: 'false', vtype: 'boolean' }
    - { question: 'sympa/use_soap', value: 'true', vtype: 'boolean' }

- name: Install softwares
  apt:
    name:
      - sympa
      - rsync
    state: present
    cache_valid_time: 600

- name: Configure Sympa default config file
  lineinfile:
    path:       /etc/sympa/sympa/sympa.conf
    regex:      "{{ item.r }}"
    line:       "{{ item.l }}"
  with_items:
    - { r: "^#?listmaster",     l: "listmaster     dave@davepage.me.uk" }
    - { r: "^#?title",          l: "title          LibIT Mailing Lists" }
    - { r: "^#?gecos",          l: "gecos          LibIT Mailing Lists" }
  notify:
    - restart-sympa
    - restart-apache

#################################################
# Daily database dump

- name:       "Daily database dump"
  copy:
    src:      "99mariadb-backup"
    dest:     "/etc/cron.daily/"
    owner:    "root"
    group:    "root"

#################################################
# Set up Apache with FastCGI

- name: "Install Apache"
  apt:
    name:
      - apache2
      - libapache2-mod-fcgid
    state: present
    cache_valid_time: 600
  notify: restart-apache

- name: "Activate apache2 modules"
  apache2_module:
    name: "{{ item }}"
    state: present
  with_items:
    - fcgid
    - ssl
    - rewrite
  notify: restart-apache

- name:       "Configure Apache security"
  lineinfile:
    dest:     "/etc/apache2/conf-enabled/security.conf"
    regexp:   "{{ item.r }}"
    line:     "{{ item.l }}"
  with_items:
    - { r: '^ServerTokens',     l: 'ServerTokens Prod' }
    - { r: '^ServerSignature',  l: 'ServerSignature Off' }
  notify:     restart-apache

- name: "Install apache config file"
  template:
    src:    '{{ ansible_fqdn }}.conf'
    dest:   '/etc/apache2/sites-available/000-default.conf'
    mode:   0644
    owner:  root
    group:  root
  notify:   restart-apache
