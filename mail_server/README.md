# `mail_server`

This role sets up a single-host mail server for virtual users. It's designed
for running directly on a Debian 11 host; the Exim and Dovecot daemons are
installed directly rather than in containers etc.

## Overview

1. Set up Exim as an MTA to receive email from the Internet.
1. Set up Dovecot as an IMAP server to serve mail to specified users
1. Share authentication between Exim and Dovecot, driven from Ansible

## Configuration

This role relies on config under `mail_server`:

```
mail_server:
  domains:
    example.org:
      user.name:    dovecotuser
  users:
    dovecotuser:    mypassword
```

This sets up incoming email to user.name@example.org to be delivered to a
Dovecot user `dovecotuser`, with the password `mypassword`. You'll want to use
ansible vault or similar to encrypt your own passwords locally.

NB: Dovecot users are of "user" rather than "user@host" format.

## Implementation

### Dovecot

These are set in `defaults/main.yml` and can be overridden.

### Exim

The data in `mail_server.domains` is put into files in `/etc/exim4/virtual/`,
and an Exim redirect router `router_virtual` maps email addresses to Dovecot
users.

An Exim accept router `router_dovecot` accepts email for existing Dovecot users
via the `transport_dovecot` transport, which uses `dovecot-lda` to deliver the
mail.
